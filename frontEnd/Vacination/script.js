$(document).ready(function () {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL = "api/v1/"
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading()
    $("#btn-get").on("click", () => {
        $(".reg-inp").focus()
    })
    $("#btn-help").on("click", () => {
        $(".quote-inp").focus()
    })
    $("#btn-verify").on("click", function () {
        let phone = $(".reg-phone").val()
        let vValidate = isPhoneValidate(phone)
        if (vValidate == true) {
            alert("Số điện thoại hợp lệ")
        }
        else {
            alert("Số điện thoại không hợp lệ")
        }
    })
    $("#btn-submit").on("click", () => {
        createUser()
    })
    $("#btn-contact").on("click", () => {
        createContact()
    })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
    }
    function createUser() {
        const vUser = {
            fullName: "",
            phone: ""
        }
        // B1 thu thập dữ liệu
        getUserData(vUser)
        // B2 Kiểm tra dữ liệu
        var vValidate = isDataValidate(vUser)
        if (vValidate == true) {
            // B3 Gọi Api xử lý
            // console.log(vUser)
            processApiToCreateUser(vUser)
        }
    }
    function createContact(){
        const vContact = {
            email:""
        }
        // B1 thu thập dữ liệu
        getContactData(vContact)
        // Kiểm tra dữ liệu
        if(isEmailValidate(vContact.email)==false){
            alert("Email không hợp lệ")
            return false
        }
        else{
            // B3 gọi Api xử lý
            processApiToCreateContact(vContact)
        }
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm thu thập thông tin để tạo user
    function getUserData(paramObject) {
        paramObject.fullName = $.trim($(".reg-inp").val())
        paramObject.phone = $.trim($(".reg-phone").val())
    }
    // Hàm kiểm tra thông tin để tạo user
    function isDataValidate(paramObject) {
        if (!paramObject.fullName) {
            alert("Hãy nhập tên")
            return false
        }
        if (isPhoneValidate(paramObject.phone) == false) {
            alert("Số điện thoại không hợp lệ")
            return false
        }
        return true
    }
    //Hàm kiểm tra SĐT
    function isPhoneValidate(paramPhone) {
        var regexPhoneNumber = /(84|0[1|3|7|9])+([0-9]{8})\b/g;
        return paramPhone.match(regexPhoneNumber) ? true : false;
    }
    // Hàm gọi Api để tạo user
    function processApiToCreateUser(paramObject) {
        $.ajax({
            url: gBASE_URL + "users/",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObject),
            // async: false,
            success: function (res) {
                // Xử lý hiển thị
                alert("Đăng ký thành công")
            },
            error: function (paramErr) {
                console.log(paramErr);
            }
        })

    }
     //Hàm thu thập dữ liệu để tạo contact
     function getContactData(paramObject) {
        paramObject.email = $.trim($(".quote-inp").val())
    }
    //Hàm kiểm tra email
    function isEmailValidate(paramEmail) {
        var vRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return vRegex.test(paramEmail)
    }
       // Hàm gọi Api để tạo user
       function processApiToCreateContact(paramObject) {
        $.ajax({
            url: gBASE_URL + "contacts/",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObject),
            // async: false,
            success: function (res) {
                // Xử lý hiển thị
                alert("Đăng ký liên hệ thành công")
            },
            error: function (paramErr) {
                console.log(paramErr);
            }
        })
}
})