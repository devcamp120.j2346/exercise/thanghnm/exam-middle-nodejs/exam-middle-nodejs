const port= 8000

const getTime = (req, res, next) => {
    let today = new Date();
    const message = `Bây giờ là ${today.getHours()} giờ ${today.getMinutes()} phút`
    console.log(message)
    next();
}

const getUrl = (req,res,next)=>{
    console.log(`localhost:${port}${req.originalUrl}`)
    next()
}
module.exports={
 getTime,
 getUrl
}