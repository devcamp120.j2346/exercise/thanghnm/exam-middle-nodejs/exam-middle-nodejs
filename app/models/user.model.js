//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    status: {
        type: String,
        default: "Level 0"
    },

},
    { timeStamp: true }
)
// Biên dịch Schema
module.exports = mongoose.model("User", userSchema)