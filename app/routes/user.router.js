// Import Thư viện Express
const express = require("express");
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/user.controller");
// Khai báo router
const router = express.Router();

// Router lấy danh sách user
router.get("/",getAllUser )
// Router lấy user bằng id
router.get("/:userId/",getUserById )
// Router tạo user
router.post("/",createUser )
// Router update user bằng id
router.put("/:userId/",updateUserById )
// Router delete user bằng id
router.delete("/:userId/",deleteUserById )
// Export
module.exports = router
