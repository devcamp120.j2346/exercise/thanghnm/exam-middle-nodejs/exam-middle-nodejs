//B1: Import thư viện express
const express = require('express');
const mongoose = require("mongoose")
//B2: Khởi tạo app express
const app = new express();
app.use(express.json())
//B3: Khai báo cổng để chạy api
const port = 8000;
// Kết nối với MongoDB:
mongoose.connect("mongodb://127.0.0.1:27017/Vaccine")
.then(()=>console.log("Connected to Mongo Successfully"))
.catch(error=>handleEror(error))
// Hiển thị hình ảnh
app.use(express.static(__dirname + "/frontEnd/Vacination"))
// Import Router
const indexRouter = require("./app/routes/index.router")
const userRouter = require("./app/routes/user.router")
const contactRouter = require("./app/routes/contact.router")

// Sử dụng router
app.use("/", indexRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/contacts", contactRouter);

//B4: Start app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})